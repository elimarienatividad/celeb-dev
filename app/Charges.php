<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charges extends Model
{
    protected $guarded = array();

    public function chglib(){
        return $this->belongsToMany('App/Chglib');
    }
}
