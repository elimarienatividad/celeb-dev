<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chglib extends Model
{
    protected $guarded = array();
    
    public function charges(){
        return $this->belongsToMany('App/Charges');
    }
}
