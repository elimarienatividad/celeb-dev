<?php

namespace App\Http\Controllers;
use App\User;
use App\Charges;
use App\Chglib;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
use Illuminate\Support\Facades\Storage;
use App\Events\ExportAllEvent;
use App\Mail\BillingEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Http;
use Response;
use Mailgun\Mailgun;

class BillingController extends Controller
{
    public function __construct(){
        //$this->middleware('auth'); //lock everything
        $this->middleware('auth')->except(['export_view', 'manual_email_send', 'manual_sms_send']); //allow guests to only see index
        //$this->middleware('auth')->only(['index']); //
    }

    public function index(){
        $users = User::where('role_id', 2)->paginate(50);
        return view('billing.index', compact('users'));
    }

    public function export($id){

    }
    
    function export_view($account_number_id, $subaccount_number_id){
        $pdf = $this->pdf($account_number_id, $subaccount_number_id);
        $filename = $pdf->name.'.pdf';
        $path = public_path($pdf->path);

        $response = Response::make(file_get_contents($path), 200);
        $response->header('Content-Type', 'application/pdf');
        $response->header('Content-Disposition', 'inline; filename="'.$filename.'"');
        return $response;
    }

    public function export_all(){
        event(new ExportAllEvent(auth()->user()));
        return redirect('billing');
        exit;
    }

    function pdf($id, $sub_id){
        //get pdf data
        $pdf = DB::table('pdf')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $pdf;
    }

    function manual_sms_send(){
        //check 1190-2 - no sms sent
        //$users = DB::table('users')->where('role_id', 2)->whereRaw("CONCAT(account_number_id, '-', subaccount_number_id) IN ('367-0','369-0','917-0','963-0','1825-0','1876-0','2483-0','3004-1','230-0','3245-0','3234-0','3283-0','829-0','3319-0','1874-0','1041-0','2805-0','1614-0','283-0','1831-0','2557-0','2257-0','1982-0','1799-0','1659-0','2781-0','2902-0','40-0','902-1','2874-0','1245-0','747-0','1238-3','2549-1','232-3','239-2','178-2','148-2','237-5','1379-2','194-4','1572-4','1797-4','2930-3','1320-3','2962-4','2302-2','9-2','311-4','1912-4','2081-3','2299-4','1335-4','2122-2','2281-5','227-1','1357-5','1746-5','1753-5','2464-2','1753-2','60-1','195-3','1133-2','1762-2','2055-2','2056-3','2951-5','3412-5')")->get();
        $users = DB::table('users')->where('role_id', 2)->get();
        $test_ctr = 0;
        foreach($users AS $get_user):
            $check_if_exists = DB::table('message_logs')->where('account_number_id', $get_user->account_number_id)->where('type', 'SMS')->where('group', 'SOA')->where('subaccount_number_id', $get_user->subaccount_number_id)->get();
            $fixed_data = $this->fixed($get_user->account_number_id, $get_user->subaccount_number_id);
            $aging_data = $this->aging($get_user->account_number_id, $get_user->subaccount_number_id);
            if(count($check_if_exists) == 0){
            //if($check_if_exists == 0){
                if($aging_data && $fixed_data){
                    //get pdf data
                    $soa_date = DB::table('soa_date')->orderBy('id', 'desc')->first();
                    echo $get_user->account_number_id.'-'.$get_user->subaccount_number_id;
                    echo '<br />';
                    $pdf = DB::table('pdf')->where('account_number_id', $get_user->account_number_id)->where('subaccount_number_id', $get_user->subaccount_number_id)->first();
                    $fixed = DB::table('fixed')->where('account_number_id', $get_user->account_number_id)->where('subaccount_number_id', $get_user->subaccount_number_id)->first();
                    //send SMS
                    if($pdf){
                        $running_balance = DB::table('aging')->where('account_number_id', $get_user->account_number_id)->where('subaccount_number_id', $get_user->subaccount_number_id)->first();
                        //$total_balance = $running_balance->computed_balance_to_date - $running_balance->new_consumable_credit;
                        $total_balance = $running_balance->total;
                        if($total_balance > 0){
                            $message = 'Hi '.number_format($total_balance, 2).' is due on '.date('m').'/30. View '.$pdf->sms_url.' for your bill. The password is account# and last name (ex. 08180reyes). Call 89513333 for help';
                        } else {
                            $message = 'Hi your balance is '.number_format($total_balance, 2).'. View '.$pdf->sms_url.' for your bill. The password is account# and last name (ex. 08180reyes). Call 89513333 for help';
                        }
                        $sms_count = strlen($message);
                        //['computed_balance_to_date' => $running_balance, 'new_consumable_credit' => $new_consumable_credit]);
                        $old_message = 'Hi, the amount to pay for Account No.'.str_pad($get_user->account_number_id, 4, '0', STR_PAD_LEFT).'-'.$get_user->subaccount_number_id.' is '.number_format($total_balance, 2).' due on '.date('m').'/30. Click '.$pdf->sms_url.' to view your SOA or call 89513333 for assistance.';
    
                        if($get_user->mobile_number && ($get_user->mobile_number != 'NULL' || $get_user->mobile_number != null || $get_user->mobile_number != '')){
                            $response = Http::post('https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/6063/requests/', [
                                'message' => $message,
                                'address' => $get_user->mobile_number,//'9175245735',//9175263700'9175245735',
                                'passphrase' => 'RhGy4fFH7U',
                                'app_id' => 'yXM4F54eEgCMLTK8MqieqkCGeXo6Fo7G',
                                'app_secret' => 'ab6c5a108cdb0dbaa6bcef99d71bd567365136f1bfb1d29acc9c2ad5f10197ee'
                            ]);
                            //add in message_logs
                            DB::table('message_logs')->insert([
                                ['account_number_id' => $get_user->account_number_id, 'subaccount_number_id' => $get_user->subaccount_number_id, 'created_at' => date('Y-m-d H:i:s'), 'type' => 'SMS', 'group' => 'SOA', 'status' => $response->status(), 'response' => $response]
                            ]);
                        }
                    }
                }
            }
        endforeach;
    }

    function manual_email_send(){
        //$users = DB::table('users')->where('role_id', 2)->whereRaw("CONCAT(account_number_id, '-', subaccount_number_id) IN ('367-0','369-0','917-0','963-0','1825-0','1876-0','2483-0','3004-1','230-0','3245-0','3234-0','3283-0','829-0','3319-0','1874-0','1041-0','2805-0','1614-0','283-0','1831-0','2557-0','2257-0','1982-0','1799-0','1659-0','2781-0','2902-0','40-0','902-1','2874-0','1245-0','747-0','1238-3','2549-1','232-3','239-2','178-2','148-2','237-5','1379-2','194-4','1572-4','1797-4','2930-3','1320-3','2962-4','2302-2','9-2','311-4','1912-4','2081-3','2299-4','1335-4','2122-2','2281-5','227-1','1357-5','1746-5','1753-5','2464-2','1753-2','60-1','195-3','1133-2','1762-2','2055-2','2056-3','2951-5','3412-5')")->get();
        $users = DB::table('users')->where('role_id', 2)->get();
        foreach($users AS $get_user):
            //$check_if_exists = 0;
            $check_if_exists = DB::table('message_logs')->where('account_number_id', $get_user->account_number_id)->where('type', 'email')->where('group', 'SOA')->where('subaccount_number_id', $get_user->subaccount_number_id)->get();
            $fixed_data = $this->fixed($get_user->account_number_id, $get_user->subaccount_number_id);
            $aging_data = $this->aging($get_user->account_number_id, $get_user->subaccount_number_id);
            if($check_if_exists == 0){
                if($aging_data && $fixed_data){
                    //get pdf data
                    $soa_date = DB::table('soa_date')->orderBy('id', 'desc')->first();
                    $pdf = DB::table('pdf')->where('account_number_id', $get_user->account_number_id)->where('subaccount_number_id', $get_user->subaccount_number_id)->first();
                    $last_name = explode(',', $fixed_data->name);
                    $first_name = explode(' ', $last_name[1]);
                    
                    $last_name = str_replace(' ', '', mb_convert_case($last_name[0], MB_CASE_LOWER, "UTF-8")); //strtolower(str_replace(' ', '', $last_name[0]));
                    //send email here
                    if($pdf){
                        if($get_user->email && ($get_user->email != 'NULL' || $get_user->email != null || $get_user->email != '')){
                            # Instantiate the client.
                            $mgClient = Mailgun::create('b9b5daf61f8c0f0c013ced2b8b103156-a83a87a9-34ce135f', 'https://api.mailgun.net/v3');
                            $domain = "email.celebritysportsplaza.com";

                            $message = '<p>Dear '.ucfirst(strtolower($first_name[1])).',</p>

                            <p>Your billing statement for the month of '.date('F Y', strtotime($soa_date->soa_date)).' is now available for viewing. You may click on the attached PDF file to view your SOA.</p>
                            
                            <p>For your protection, the attached PDF file is password-protected. To open the said file, please type in your default password.  Your default password is your 5 to 6-digit account number and your last name (example: 12341reyes).</p>
                            
                            <p>You may settle your monthly billing statement through any of the following Payment Channels.</p>
                            <ul>
                                <li>Club Cashier</li>
                                <li>BDO Branches</li>
                                <li>BDO Online</li>
                                <li>Eastwest Bank Branches</li>
                                <li>Eastwest Online</li>
                                <li>Paymaya</li>
                            </ul>
                            <p>This is a system-generated e-mail. Please DO NOT REPLY. For inquiries and other concerns, you may e-mail us at billing.celebrityclub@gmail.com or call our billing department at 8951-3333 loc. 121/122.</p>
                            <div style="height: 75px;"></div>
                            <p><span style="color:red;">DATA PRIVACY NOTICE:</span> The information contained in this communication, including its attachments, is intended solely for the use of the individual or entity to whom it is addressed and other authorized to receive it. It may contain confidential or legally privileged information. If you are not the intended recipient, you are hereby notified that any disclosure, copying, distribution or taking of any action in reliance on the content of this communication in error, is legally actionable.  Please notify the sender immediately by responding to his email and then deleting it from your system.</p>';

                            $params = array(
                            'from'    => 'Celebrity Sports Plaza <noreply@email.celebritysportsplaza.com>',
                            'to'      => $get_user->email,
                            //'bcc'      => 'eli@creativequoin.com',
                            'subject' => 'Billing Email',
                            'html'    => $message,
                            'attachment' => [
                                [
                                    'filePath' => public_path('storage/uploads/attachments/Celebrity_Sports_Club-August_Circular.pdf'),
                                    'filename' => 'Celebrity_Sports_Club-August_Circular.pdf'
                                ],
                                [
                                    'filePath' => public_path('storage/uploads/attachments/Celebrity_Sports_Club-Employee_Donation_List.pdf'),
                                    'filename' => 'Celebrity_Sports_Club-Employee_Donation_List.pdf'
                                ],
                                [
                                    'filePath' => public_path('storage/uploads/attachments/Celebrity_Sports_Club-Sponsorship.pdf'),
                                    'filename' => 'Celebrity_Sports_Club-Sponsorship.pdf'
                                ],
                                [
                                    'filePath' => public_path('storage/uploads/attachments/Paymaya.pdf'),
                                    'filename' => 'Paymaya.pdf'
                                ],
                                [
                                    'filePath' => public_path('storage/uploads/pdf/'.date('Y-m', strtotime($soa_date->soa_date)).'/'.$pdf->name.'.pdf'),
                                    'filename' => $pdf->name.'.pdf'
                                ]
                            ],
                            );

                            # Make the call to the client.
                            echo $get_user->email;
                            $response = $mgClient->messages()->send($domain, $params);
                            
                            //add in message_logs
                            DB::table('message_logs')->insert([
                                ['account_number_id' => $get_user->account_number_id, 'subaccount_number_id' => $get_user->subaccount_number_id, 'created_at' => date('Y-m-d H:i:s'), 'type' => 'email', 'group' => 'SOA', 'message_id' => $response->getId(), 'response' => $response->getMessage()]
                            ]);
                            sleep(40);
                        }
                    }
                }
            }
        endforeach;
    }

    function fixed($id, $sub_id){
        //get fixed data
        $fixed = DB::table('fixed')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $fixed;
    }

    function aging($id, $sub_id){
        //get aging data
        $aging = DB::table('aging')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $aging;
    }
}
