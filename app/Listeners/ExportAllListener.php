<?php

namespace App\Listeners;

use App\Events\ExportAllEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Charges;
use App\Chglib;
use App\Billing;
use PDF;
use App\Mail\BillingEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Http;

class ExportAllListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  ExportAllEvent  $event
     * @return void
     */
    public function handle(ExportAllEvent $event)
    {        
        
        $users = $this->user();
        //start loop here
        $soa_date = $this->soa_date();
        
        //foreach($users AS $user):
            //$account_number_id = $user->account_number_id;
            //$subaccount_number_id = $user->subaccount_number_id;
            $mdbilled_all = $this->mdbilled();
            foreach($mdbilled_all AS $mdbilled):
                $id = $mdbilled->account_number_id;
                $sub_id = $mdbilled->subaccount_number_id;
                $fixed_data = $this->fixed($id, $sub_id);
                $aging_data = $this->aging($id, $sub_id);
                $charges_data = $this->charges($id, $sub_id);
                $csmbluse_data = $this->csmbluse($id, $sub_id);
                $csmblcr_data = $this->csmblcr($id, $sub_id);
                $prevbal_data = $this->prevbal($id, $sub_id);
                $payment_data = $this->payment($id, $sub_id);
                $interest_data = $this->interest($id, $sub_id);
                //$mdbilled_data = $this->mdbilled($id, $sub_id);
                $jv_data = $this->jv($id, $sub_id);
                $mdpa_data = $this->mdpa($id, $sub_id);
                
                //check if the pdf is available
                $check_if_exists = DB::table('pdf')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->get();
                if(count($check_if_exists) == 0){
                    //if($aging_data && $csmblcr_data && $fixed_data){
                    if($aging_data && $fixed_data){
                        //compute balance to date
                        $running_balance = 0;
                        if($prevbal_data) { //start previous balance
                            $running_balance = floatval($prevbal_data->total);
                        }
                        //end previous balance
                        if($payment_data) { //start payment
                            foreach($payment_data AS $payment):
                            $running_balance = floatval($running_balance - $payment->amount);
                            endforeach;
                        }
                        //end payment
                        if($interest_data) { //start interest
                            $running_balance = floatval($running_balance + $interest_data->interest_amount);
                            if($interest_data->vat_interest > 0) {
                                $running_balance = floatval($running_balance + $interest_data->vat_interest);
                            } //end vat on interest
                        } //end interest
                        if($mdbilled) { //start mdbilled
                            $running_balance = floatval($running_balance + $mdbilled->monthly_dues);
                        } //end mdbilled
                        if($jv_data) { //start jv
                            $running_balance = $running_balance - $jv_data->amount;
                        } //end jv
                        if($charges_data) { //start charges
                            foreach($charges_data AS $charges):
                                $running_balance = $running_balance + floatval($charges->amount);
                            endforeach;
                        } //end charges
                        
                        $new_consumable_credit =  floatval($running_balance - $aging_data->total);
                        
                        DB::table('aging')->where('account_number_id', $fixed_data->account_number_id)->where('subaccount_number_id', $fixed_data->subaccount_number_id)->update(['computed_balance_to_date' => $running_balance, 'new_consumable_credit' => $new_consumable_credit]);
                        
                        //explode last name
                        $last_name = explode(',', $fixed_data->name);
                        $first_name = explode(' ', $last_name[1]);
                        echo 'Password: '.str_pad($fixed_data->account_number_id, 4, '0', STR_PAD_LEFT).$fixed_data->subaccount_number_id.str_replace("ñ", "n", mb_convert_case($last_name[0], MB_CASE_LOWER, "UTF-8"));
                        
                        PDF::SetProtection(array('print', 'copy','modify'), str_pad($fixed_data->account_number_id, 4, '0', STR_PAD_LEFT).$fixed_data->subaccount_number_id.str_replace("ñ", "n", mb_convert_case($last_name[0], MB_CASE_LOWER, "UTF-8")), "ei2RwMDACPeJu5AqzMDo", 0, null);
                        PDF::SetFont('courier', '', 10);
                        // Custom Header
                        $header_text = \View::make('billing.header', ['fixed' => $fixed_data, 'aging' => $aging_data, 'soa_date' => $soa_date->soa_date]);
                        
                        PDF::setHeaderData($ln='', $lw=0, $ht='', $header_text, $tc=array(0,0,0), $lc=array(0,0,0));
                        PDF::setFooterData(array(0,64,0), array(0,64,128));
                        PDF::setHeaderCallback(function($pdf) {
                            // Line break
                            $pdf->Ln(10);
                            // Set font
                            $pdf->SetFont('courier', '', 10);
                            // Titleheader_string
                            //$pdf->Cell(0, 15, 'Something new right here!!!', 0, false, 'C', 0, '', 0, false, 'M', 'M');
                            $headerData = $pdf->getHeaderData();
                            //$pdf->Cell(0, 15, $headerData['string'], 0, false, 'C', 0, '', 0, false, 'M', 'M');
                            $pdf->writeHTML($headerData['string'], true, false, true, false, '');
                        });
                        // Custom Footer
                        PDF::setFooterCallback(function($pdf) use ($mdpa_data, $csmblcr_data) {
                
                                // Position at 15 mm from bottom
                                $pdf->SetY(-15);
                                // Set font
                                $pdf->SetFont('courier', '', 10);
                                // Page number
                                
                                //$pdf->Cell(0, 10, 'Page '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
                                $page = $pdf->getAliasNumPage();
                                $pages = $pdf->getAliasNbPages();
                                $footer_text = \View::make('billing.footer', ['mdpa' => $mdpa_data, 'csmblcr' => $csmblcr_data, 'page' => $page, 'pages' => $pages]);
                                $pdf->writeHTML($footer_text);
                
                        });
                        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
                        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
                        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
                        PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);     
                        PDF::AddPage('P', 'A4');
                        PDF::Ln(5);
                        $view = \View::make('billing.template', ['fixed' => $fixed_data, 'aging' => $aging_data, 'prevbal' => $prevbal_data, 'payment' => $payment_data, 'interest' => $interest_data, 'mdbilled' => $mdbilled, 'jv' => $jv_data, 'charges' => $charges_data, 'csmbluse' => $csmbluse_data, 'csmblcr' => $csmblcr_data, 'soa_date' => $soa_date->soa_date, 'new_consumable_credit' => $new_consumable_credit]);
                        $html = $view->render();
                        PDF::writeHTML($html, true, false, true, false, '');
                        PDF::lastPage();
                        $seed = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()';
                        
                        //check if the filename is taken
                        do {
                            $filename = str_pad($fixed_data->account_number_id, 4, '0', STR_PAD_LEFT).$fixed_data->subaccount_number_id.date('Y').date('m').date('d').substr(str_shuffle($seed), 0, 5);
                            $filename = strtr(base64_encode($filename), '+/=', '-_~');
                            $data = DB::table('pdf')->where('name', $filename )->get();
                        }
                        while ($data->count());
                        //save to pdf;
                        
                        $sms_url = file_get_contents('https://tinyurl.com/api-create.php?url=https://online.celebritysportsplaza.com//billing/export/view/'.$fixed_data->account_number_id.'/'.$fixed_data->subaccount_number_id);
                        
                        
                        DB::table('pdf')->insert([
                            ['name' => $filename, 'account_number_id' => $fixed_data->account_number_id, 'subaccount_number_id' => $fixed_data->subaccount_number_id, 'created_at' => date('Y-m-d H:i:s'), 'path' => 'storage/uploads/pdf/'.date('Y-m', strtotime($soa_date->soa_date)).'/'.$filename.'.pdf', 'created_by' => $event->user->id, 'updated_by' => $event->user->id, 'sms_url' => $sms_url]
                        ]);
                        //$path = '/public/uploads/test/'.str_pad($fixed_data->account_number_id, 4, '0', STR_PAD_LEFT).'_'.date('Y').'_'.date('m');
                        $path = '/public/uploads/pdf/'.date('Y-m', strtotime($soa_date->soa_date));
                        
                        if(!Storage::exists($path)) {
                
                            Storage::makeDirectory($path, 0777, true); //creates directory
                        
                        }
                        
                        //PDF::Output(public_path('storage/uploads/pdf/'.str_pad($fixed_data->account_number_id, 4, '0', STR_PAD_LEFT).'_'.date('Y').'_'.date('m').'/'.$filename.'.pdf'), 'F');
                        PDF::Output(public_path('storage/uploads/pdf/'.date('Y-m', strtotime($soa_date->soa_date)).'/'.$filename.'.pdf'), 'F');
                        PDF::reset();
                    }
                }
            endforeach;
        //endforeach;
    }
    
    function base64_safe_decode($input) {
        return base64_decode(strtr($input, '-_~', '+/='));
    }

    function fixed($id, $sub_id){
        //get fixed data
        $fixed = DB::table('fixed')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $fixed;
    }

    function aging($id, $sub_id){
        //get aging data
        $aging = DB::table('aging')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $aging;
    }

    function prevbal($id, $sub_id){
        $prevbal = DB::table('prevbal')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $prevbal;
    }

    function payment($id, $sub_id){
        $payment = DB::table('payment')->select(DB::raw("payment_date, reference_no, amount, (CASE WHEN reference_no LIKE 'CM%' THEN 'Credit Memo' ELSE 'Payment - Thank You' END) AS description"))->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->orderBy('payment_date', 'asc')->get();
        return $payment;
    }

    function interest($id, $sub_id){
        $interest = DB::table('interest')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $interest;
    }

    function mdbilled($id = false, $sub_id = false){
        if($id){
            $mdbilled = DB::table('mdbilled')->where('account_number_id', $sub_id)->where('subaccount_number_id', $sub_id)->first();
        } else {
            $mdbilled = DB::table('mdbilled')->where('monthly_dues', '>', 0)->orderBy('account_number_id', 'asc')->orderBy('subaccount_number_id', 'asc')->get();
            //$mdbilled = DB::table('mdbilled')->where('monthly_dues', '>', 0)->whereIn('account_number_id', ['2439','0013','0016','0026','0146','0185','1846','2057','3367','0190','0735','1018','1266','1323','2396','2697','2956','2989','3434'])->orderBy('account_number_id', 'asc')->orderBy('subaccount_number_id', 'asc')->get();
        }
        return $mdbilled;
    }

    function jv($id, $sub_id){
        $jv = DB::table('jv')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $jv;
    }

    function charges($id, $sub_id){
        $charges = Charges::where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->leftJoin('chglib', 'charges.code', '=', 'chglib.code')->orderBy('charge_number', 'asc')->orderBy('charge_date', 'asc')->get();
        return $charges;
    }

    function csmbluse($id, $sub_id){
        $csmbluse = DB::table('csmbluse')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $csmbluse;
    }

    function csmblcr($id, $sub_id){
        $csmblcr = DB::table('csmblcr')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $csmblcr;
    }

    function mdpa($id, $sub_id){
        $mdpa = DB::table('mdpa')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $mdpa;
    }

    function user($id = false, $sub_id = false){
        if($id){
            $user = DB::table('users')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        } else {
            $user = DB::table('users')->where('role_id', 2)->get();
        }
        return $user;
    }

    function soa_date(){
        return DB::table('soa_date')->orderBy('id', 'desc')->first();
    }
}
