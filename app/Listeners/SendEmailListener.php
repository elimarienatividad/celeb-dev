<?php

namespace App\Listeners;

use App\Events\ExportAllEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\BillingEmail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SendEmailListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExportAllEvent  $event
     * @return void
     */
    public function handle(ExportAllEvent $event)
    {
        $users = $this->users();
        foreach($users AS $users):
            $user_array = ['password' => ''];
            Mail::send(new BillingEmail());
        endforeach;
    }

    function users(){
        $users = DB::table('users')->whereNotNull('account_number_id')->whereNotNull('email')->where('role_id', 2)->get();
        return $users;
    }

    function fixed($id, $sub_id){
        //get fixed data
        $fixed = DB::table('fixed')->where('account_number_id', $id)->where('subaccount_number_id', $sub_id)->first();
        return $fixed;
    }
}
