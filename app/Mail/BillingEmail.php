<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BillingEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_data)
    {
        $this->user_data = $user_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //live to: $this->user_data['email'] cspi.csanchez@gmail.com'
        return $this->to($this->user_data['email'])
            ->view('emails.billing-mailgun')
            ->with([
                'name' => $this->user_data['first_name'],
                'password' => $this->user_data['password'],
                'email' => $this->user_data['email']
                ])
            ->attach($this->user_data['attachment1'], [
                'as' => $this->user_data['attachment1_name'],
                'mime' => 'application/pdf',
            ])
            ->attach($this->user_data['attachment2'], [
                'as' => $this->user_data['attachment2_name'],
                'mime' => 'application/pdf',
            ])
            ->attach($this->user_data['attachment3'], [
                'as' => $this->user_data['attachment3_name'],
                'mime' => 'application/pdf',
            ])
            ->attach($this->user_data['attachment4'], [
                'as' => $this->user_data['attachment4_name'],
                'mime' => 'application/pdf',
            ])
            ->attach($this->user_data['pdf'], [
                'as' => $this->user_data['pdf_name'],
                'mime' => 'application/pdf',
            ]);
    }
}
