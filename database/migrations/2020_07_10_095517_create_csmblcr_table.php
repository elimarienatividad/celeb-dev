<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCsmblcrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csmblcr', function (Blueprint $table) {
            $table->id();
            $table->integer('account_number_id')->nullable();
            $table->integer('subaccount_number_id')->nullable();
            $table->decimal('amount',10,2)->default(0);
            $table->date('expiry_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csmblcr');
    }
}
