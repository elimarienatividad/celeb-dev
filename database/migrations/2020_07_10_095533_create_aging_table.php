<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aging', function (Blueprint $table) {
            $table->id();
            $table->integer('account_number_id')->nullable();
            $table->integer('subaccount_number_id')->nullable();
            $table->decimal('current',10,2)->default(0);
            $table->decimal('m_30_days',10,2)->default(0);
            $table->decimal('m_60_days',10,2)->default(0);
            $table->decimal('m_90_days',10,2)->default(0);
            $table->decimal('m_120_days',10,2)->default(0);
            $table->decimal('m_150_days',10,2)->default(0);
            $table->decimal('total',10,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aging');
    }
}
