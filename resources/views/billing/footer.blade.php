<style>
    table {
        font-size: 8px;
        text-align: left;
    }
    .special {
        text-align: center;
    }
    .for-number {
        text-align: right;
    }
    p {
        font-size: 8px;
    }
</style>
<table>
    <tbody>
        @if($mdpa && $mdpa->amount > 0)
        <tr>
            <td></td>
            <td></td>
            <td style="text-align:left;">MDPA Bal.</td>
            <td class="for-number">{{ number_format($mdpa->amount, 2) }}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="text-align:left;">VAT on MDPA Bal.</td>
            <td></td>
        </tr>
        @endif
        @if($csmblcr)
        <tr>
            <td style="text-align:left;">Consumable Balance</td>
            <td style="text-align:left;">{{ number_format($csmblcr->amount, 2) }}</td>
            <td style="text-align:left;"> Valid until {{ $csmblcr->expiry_date }}</td>
            <td></td>
        </tr>
        @else
        <tr>
            <td style="text-align:left;">Consumable Balance</td>
            <td style="text-align:left;">{{ number_format(0, 2) }}</td>
            <td style="text-align:left;"></td>
            <td></td>
        </tr>
        @endif
    </tbody>
</table>
<hr />
<table>
    <tbody>
        <tr>
            <td width="100">{{ date('m/d/Y') }}</td>
            <td width="100">{{ date('H:i:s') }}</td>
            <td width="360" class="for-number">Page {{$page}}/{{$pages}}</td>
        </tr>
    </tbody>
</table>