
<table>
    <thead>
        <tr>
            <th width="120"><img src="https://online.celebritysportsplaza.com/storage/uploads/celeb_logo.jpg" alt="test alt attribute" width="120" height="80" border="0" /></th>
            <th width="300" style="text-align:center;">
                <strong>The Celebrity Club</strong>
                <br />
                <strong>Capitol Hills Drive, Diliman, Quezon City</strong>
                <br />
                <strong>Tel. No. 8951-3333, 8931-0911</strong>
                <br />
                e-mail: billing.celebrityclub@gmail.com
            </th>
            <th></th>
        </tr> 
    </thead>
</table>
<table>
    <thead>
        <tr>
            <th width="120"></th>
            <th width="300" style="text-align:center;"><strong>STATEMENT OF ACCOUNT</strong></th>
            <th></th>
        </tr>
    </thead>
</table>
<div style="height:20px;">&nbsp;</div>
<table>
    <thead>
        <tr>
            <th width="120"><strong>{{ str_pad($fixed->account_number_id, 4, '0', STR_PAD_LEFT) }} {{ $fixed->subaccount_number_id }}</strong></th>
            <th width="300"><span style="text-align:center;"><strong>VAT Reg. TIN# 000-677-390-000 VAT</strong></span></th>
            <th width="70" style="text-align:right;">{{ date("m/d/Y", strtotime($soa_date)) }}</th>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <th colspan="3"><strong>{{ $fixed->name }}</strong></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">{{ $fixed->address }}</td>
        </tr>
    </tbody>
</table>
<div style="height:20px;">&nbsp;</div>
<table style="border-top: 3px solid black;font-size: 8px;">
    <thead>
        <tr>
            <th cellpadding="5" style="text-align:center;">Current</th>
            <th cellpadding="5" style="text-align:center;">Over 30 Days</th>
            <th cellpadding="5" style="text-align:center;">Over 60 Days</th>
            <th cellpadding="5" style="text-align:center;">Over 90 Days</th>
            <th cellpadding="5" style="text-align:center;">Over 120 Days</th>
            <th cellpadding="5" style="text-align:center;">Over 150 Days</th>
            <th cellpadding="5" style="text-align:center;">Total</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td cellpadding="5" style="text-align:center;">{{ number_format($aging->current, 2) }}</td>
            <td cellpadding="5" style="text-align:center;">{{ number_format($aging->m_30_days, 2) }}</td>
            <td cellpadding="5" style="text-align:center;">{{ number_format($aging->m_60_days, 2) }}</td>
            <td cellpadding="5" style="text-align:center;">{{ number_format($aging->m_90_days, 2) }}</td>
            <td cellpadding="5" style="text-align:center;">{{ number_format($aging->m_120_days, 2) }}</td>
            <td cellpadding="5" style="text-align:center;">{{ number_format($aging->m_150_days, 2) }}</td>
            <td cellpadding="5" style="text-align:center;">{{ number_format($aging->total, 2) }}</td>
        </tr>
    </tbody>
</table>
<div style="height:20px;">&nbsp;</div>
<table style="border-top: 3px solid black; font-size: 8px;">
    <thead>
        <tr>
            <th colspan="2" cellpadding="5" style="text-align:center;">Date</th>
            <th width="180px;" colspan="2" cellpadding="5" style="text-align:center;">Description</th>
            <th width="55px;" cellpadding="5" style="text-align:right;">Debit</th>
            <th width="60px;" cellpadding="5" style="text-align:right;">Credit</th>
            <th width="60px;" cellpadding="5" style="text-align:right;">Balance</th>
        </tr>
    </thead>
</table>