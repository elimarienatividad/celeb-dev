@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                    Billing
                    <a type="button" class="btn btn-success float-right" href="{{ url('/billing/create') }}">Upload</a>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <img src="{{asset('storage/uploads/celeb_logo.jpg')}}" alt="test alt attribute" width="120" height="80" border="0" />
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Month</th>
                                <th>Year</th>
                                <th>Count</th>
                                <th>Buttons</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><button class="btn btn-celeb-blue"><i class="fa fa-plus light"></i></span></button></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    {{--
                    <!--customer list-->
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-bordered">
                                <thead>
                                    <th>Account</th>
                                    <th>Sub-account</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    @foreach($users AS $user)
                                        <td>{{$user->account_number_id}}</td>
                                        <td>{{$user->subaccount_number_id}}</td>
                                        <td>{{$user->full_name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->mobile_number}}</td>
                                        <td><a href="/billing/export/{{$user->account_number_id}}/{{$user->subaccount_number_id}}" class="btn btn-primary">Export</a></td>
                                    <tr>
                                        @endforeach
                                </tbody>
                            </table>
                            <!--PAGINATION-->
                            <div class="row">
                                <div class="col-12 text-center d-flex justify-content-center">
                                    {{ $users->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                    --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection