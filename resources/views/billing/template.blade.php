<style>
    table {
        font-size: 8px;
        text-align: left;
    }
    .special {
        text-align: center;
    }
    .for-number {
        text-align: right;
    }
    p {
        font-size: 8px;
    }
</style>
<table>
    <tbody>
        @php
        $running_balance = 0
        @endphp
        <tr>
            <td>{{ str_pad($fixed->account_number_id, 4, '0', STR_PAD_LEFT) }} {{ $fixed->subaccount_number_id }}</td>
            <td>0</td>
            <td width="180px;"  colspan="2"></td>
            <td width="55px;" ></td>
            <td width="60px;" ></td>
            <td width="60px;" ></td>
        </tr>
        @if($prevbal) {{-- start previous balance --}}
        <tr>
            <td></td>
            <td></td>
            <td width="180px;" colspan="2">Previous Balance</td>
            <td width="55px;" class="for-number">{{ $debit = ($prevbal->total > 0) ? number_format($prevbal->total, 2):'' }}</td>
            <td width="60px;" class="for-number">{{ $credit = ($prevbal->total < 0) ? number_format(abs($prevbal->total), 2):'' }}</td>
            <td width="60px;" class="for-number">{{ number_format($prevbal->total, 2) }}</td>
        </tr>
        @php
        $running_balance = floatval($prevbal->total)
        @endphp
        @endif {{-- end previous balance --}}
        @if($payment) {{-- start payment --}}
        @foreach($payment AS $payment)
        <tr>
            <td>{{ date("m/d/Y", strtotime($payment->payment_date)) }}</td>
            <td>{{ $payment->reference_no }}</td>
            <td width="180px;" colspan="2">{{ $payment->description }}</td>
            <td width="55px;" class="for-number"></td>
            <td width="60px;" class="for-number">{{ number_format($payment->amount, 2) }}</td>
            <td width="60px;" class="for-number">{{ $total = number_format(($running_balance - $payment->amount), 2) }}</td> {{--previous balance minus payment--}}
        </tr>
        @php
        $running_balance = floatval($running_balance - $payment->amount)
        @endphp
        @endforeach
        @endif {{-- end payment --}}
        @if($interest) {{-- start interest --}}
        <tr>
            <td></td>
            <td></td>
            <td width="180px;" >Interest</td>
            <td width="55px;" class="for-number">{{ number_format($interest->interest_amount, 2) }}</td>
            <td width="60px;" class="for-number"></td>
            <td width="60px;" class="for-number">{{ $interest_total = number_format(($running_balance + $interest->interest_amount), 2) }}</td>
        </tr>
        @php
        $running_balance = floatval($running_balance + $interest->interest_amount)
        @endphp
        @if($interest->vat_interest > 0)
        <tr>
            <td></td>
            <td></td>
            <td width="180px;" >VAT ON Int.</td>
            <td width="55px;" class="for-number">{{ number_format($interest->vat_interest, 2) }}</td>
            <td width="60px;" class="for-number"></td>
            <td width="60px;" class="for-number">{{ $interest_total = number_format(($running_balance + $interest->vat_interest), 2) }}</td>
        </tr>
        @endif
        @php
        $running_balance = floatval($running_balance + $interest->vat_interest)
        @endphp
        @endif {{-- end interest--}}
        @if($mdbilled) {{-- start mdbilled --}}
        <tr>
            <td></td>
            <td></td>
            <td width="180px;" colspan="2">monthly dues for - {{ strtoupper(date('F', strtotime($soa_date . " +1 month"))) }}</td>
            <td width="55px;" class="for-number">{{ number_format($mdbilled->monthly_dues, 2) }}</td>
            <td width="60px;" class="for-number"></td>
            <td width="60px;" class="for-number">{{$monthly_dues_total = number_format(($running_balance + $mdbilled->monthly_dues), 2) }}</td>
        </tr>
        @php
        $running_balance = floatval($running_balance + $mdbilled->monthly_dues)
        @endphp
        @endif {{-- end mdbilled --}}
        @if($jv) {{-- start jv --}}
        @php
        $running_balance = $running_balance - $jv->amount
        @endphp
        <tr>
            <td>{{ date("m/d/Y", strtotime($jv->jv_date)) }}</td>
            <td>{{ $jv->reference_no }}</td>
            <td width="180px;" colspan="2">Advance Dues/VAT</td>
            <td width="55px;" class="for-number"></td>
            <td width="60px;" class="for-number">{{ number_format($jv->amount, 2) }}</td>
            <td width="60px;" class="for-number">{{ $jv_total = number_format(($running_balance), 2) }}</td>
        </tr>
        @endif
        @if($charges) {{-- start charges --}}
            @foreach($charges AS $charges)
                @if ($loop->first) {{-- first row --}}
                    @php
                    $initial_charge_number = $charges->charge_number
                    @endphp
                    <tr>
                        <td>{{ str_pad($charges->account_number_id, 4, '0', STR_PAD_LEFT) }} {{ $charges->subaccount_number_id }}</td>
                        <td>{{ $charges->charge_number }}</td>
                        <td width="180px;"  colspan="2"></td>
                        <td width="55px;" ></td>
                        <td width="60px;" ></td>
                        <td width="60px;" ></td>
                    </tr>
                @endif {{-- end first row --}}
                @if($initial_charge_number != $charges->charge_number)
                @php
                $initial_charge_number = $charges->charge_number 
                @endphp
                <tr>
                    <td colspan="7"></td>
                </tr>
                <tr>
                    <td colspan="7"></td>
                </tr>
                <tr>
                    <td>{{ str_pad($charges->account_number_id, 4, '0', STR_PAD_LEFT) }} {{ $charges->subaccount_number_id }}</td>
                    <td>{{ $charges->charge_number }}</td>
                    <td width="180px;" colspan="2"></td>
                    <td width="55px;" ></td>
                    <td width="60px;" ></td>
                    <td width="60px;" ></td>
                </tr>
                @endif
                @php
                $running_balance = $running_balance + floatval($charges->amount)
                @endphp
                <tr nobr="true">
                    <td>{{ date("m/d/Y", strtotime($charges->charge_date)) }}</td>
                    <td>{{ $charges->reference_number }}</td>
                    <td width="180px;"  colspan="2">{{ $charges->description }}</td>
                    <td width="55px;" class="for-number">{{ number_format($charges->amount, 2) }}</td>
                    <td width="60px;" class="for-number"></td>
                    <td width="60px;" class="for-number">{{ number_format($running_balance, 2) }}</td>
                </tr>
            @endforeach
        @endif {{-- end charges --}}
        {{--@if($csmbluse)--}} {{-- start csmbluse --}}
        @php
        //$running_balance = $running_balance - floatval($csmbluse->amount)
        $running_balance = $running_balance - $new_consumable_credit
        @endphp
        <tr>
            <td colspan="7"></td>
        </tr>
        <tr>
            <td colspan="7"></td>
        </tr>
        <tr>
            <td>{{ str_pad($fixed->account_number_id, 4, '0', STR_PAD_LEFT) }} {{ $fixed->subaccount_number_id }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td width="180px;"  colspan="2">Consumable Credit</td>
            <td width="55px;" class="for-number"></td>
            <td width="60px;" class="for-number">{{ number_format($new_consumable_credit, 2) }} {{--{{ number_format($csmbluse->amount, 2) }}--}}</td>
            <td width="60px;" class="for-number">{{ number_format($running_balance, 2) }}</td>
        </tr>
        {{--@endif--}} {{-- end csmbluse --}}
        <tr>
            <td></td>
            <td></td>
            <td width="180px;"  colspan="2">Balance to date</td>
            <td width="55px;" class="for-number">{{ date("m/d/Y", strtotime($soa_date)) }}</td>
            <td width="60px;" class="for-number"></td>
            <td width="60px;" class="for-number">{{ number_format($running_balance, 2) }}</td>
        </tr>
    </tbody>
</table>
<div style="height:20px;">&nbsp;</div>
<div nobr="true">
    <p><strong>IMPORTANT REMINDERS</strong></p>
    <p>ADVISORY:</p>
    <p>Dear Members,</p>
    <p>Please keep all charge invoices as part of your personal records. All Statement of Accounts (SOAs) shall be deemed correct and final if no concerns are raised within 20 days receipt.</p>
    <p>Regardless of the amount, a PHP 50.00/invoice fee shall be charged to any member who wishes to retrieve the signed invoices for a certain period.</p>
    <p>Please be guided accordingly.</p>
</div>
@if($aging->m_30_days > 0 || $aging->m_60_days > 0 || $aging->m_90_days > 0 || $aging->m_120_days > 0 || $aging->m_150_days > 0)
<div nobr="true">
    <p>NOTICE OF SUSPENSION OF ACCOUNT:</p>
    <p>Dear Member,</p>
    <p>We strongly advise you to pay your OVERDUE AMOUNT on or before the end of the month to avoid the inconvenience of a suspension of account and posting of name as a delinquent member at the Club’s Bulletin Board. (Please see Article 12 of the Club’s By-Laws.)</p>
    <p>If non-payment has been for a duration of 120 days and over, the acceptance of payment will be subject to the approval of Senior Management Committee before account is re-activated.</p>
    <p>Please disregard this notice if payment has been made.</p>
</div>
@endif
{{--NOTES--}}
<div nobr="true">
   <p>NOTES:</p> 
   <ol>
       <li>Please settle account on or before the 30th of the month to avoid 3% compounded interest per month on unpaid accounts.</li>
       <li>For members who have paid dues on a monthly basis, the monthly consumable amount may be used up within the month.</li>
       <li><strong>OVERDUE ACCOUNTS.</strong> The Club shall post on the Club’s bulletin board as “Delinquent Members” those members with due accounts of 60 days and over.</li>
       <li>The Club shall suspend the member and prevent them from using the facilities if they have overdue accounts of 60 days and over, or have reached a maximum limit of PHP 20,000.00, regardless of whether current or 30 days overdue, whichever comes first.</li>
       <li>The members may settle their account through the following payment channels:</p>
           <ol type="a">
               <li>Club Cashier</li>
               <li>Over the counter payments at any Banco De Oro or East West Bank branch</li>
               <li>Online banking with Banco De Oro or East West Bank</li>
               <li>Paymaya</li>
           </ol>
       </li>
       <li>For check payments, please make check payable to "Celebrity Sports Plaza, Inc." Please indicate the following at the back of your check:
            <ol type="a">
                <li>Name and account no.</li>
                <li>Telephone no. and/or e-mail address</li>
            </ol>
       </li>
       <li>Please notify the Club immediately for any discrepancy in your Billing Statement.</li>
       <li>The Club strictly implements the "NO CARD, NO SERVICE" and "NO PASS, NO PLAY" policy. Please secure your membership cards at the Membership Office to avoid inconvenience.</li>
       <li>The Club accepts payments of ADVANCED DUES on a calendar basis only.</li>
       <li>For members who paid their dues one year in advance, the members may use the consumable portion until the end of the calendar year.</li>
       <li>The Club shall endorse overdue accounts of 120 days and over for legal action (auction and/or forfeiture).</li>
   </ol>
</div>