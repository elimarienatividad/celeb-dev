<p>Dear {{ ucfirst($name) }},</p>

<p>Your billing statement for the month of July 2020 is now available for viewing. You may click on the attached PDF file to view your SOA.</p>

<p>For your protection, the attached PDF file is password-protected. To open the said file, please type in your default password.  Your default password is your 5 to 6-digit account number and your last name (example: 12341reyes).</p>

<p>You may settle your monthly billing statement through any of the following Payment Channels.</p>
<ul>
    <li>Club Cashier</li>
    <li>BDO Branches</li>
    <li>BDO Online</li>
    <li>Eastwest Bank Branches</li>
    <li>Eastwest Online</li>
    {{--<li>BPI Online</li>--}}
    <li>Paymaya</li>
    {{--<li>GCash</li>--}}
</ul>
<p>This is a system-generated e-mail. Please DO NOT REPLY. For inquiries and other concerns, you may e-mail us at billing.celebrityclub@gmail.com or call our billing department at 8951-3333 loc. 121/122.</p>
<div style="height: 75px;"></div>
<p><span style="color:red;">DATA PRIVACY NOTICE:</span> The information contained in this communication, including its attachments, is intended solely for the use of the individual or entity to whom it is addressed and other authorized to receive it. It may contain confidential or legally privileged information. If you are not the intended recipient, you are hereby notified that any disclosure, copying, distribution or taking of any action in reliance on the content of this communication in error, is legally actionable.  Please notify the sender immediately by responding to his email and then deleting it from your system.</p>