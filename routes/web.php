<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::view('contact', 'contact');
//Route::resource('billing', 'BillingController');
Route::get('billing', 'BillingController@index')->name('billing');
//Route::get('billing/export/{id}', 'BillingController@export')->name('billing.export');
Route::get('billing/export/all', 'BillingController@export_all')->name('billing.export');
Route::get('billing/export/test', 'BillingController@export_test');//->name('billing.export');
Route::get('billing/export/sms', 'BillingController@manual_sms_send');//->name('billing.export');
Route::get('billing/export/email', 'BillingController@manual_email_send');//->name('billing.export');
Route::get('billing/export/view/{account_number_id}/{subaccount_number_id}', 'BillingController@export_view');
Route::post('mailgun/callback/{status}', 'MailgunController@callback');
/*
Route::get('billing', function() {
    return view('billing.form');
});
*/